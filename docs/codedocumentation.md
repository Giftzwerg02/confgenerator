# ConfGenerator - Documentation

## Table of Contents

1. [Class-Diagram](#class-diagram)
2. [TopologyReader](#topologyreader)
2.1. [Input-File](#input-file)
3. [Output-Files](#output-files)

## Class-Diagram
![](class_diagram.png)

## TopologyReader

In order to create proper config files we will feed the **TopologyReader** the contents of a file which it retrieves the base information from.

### Input-File
The input File needs to have proper syntax so that our **TopologyReader** understand it.

The syntax consists of multiple components that are split up into multiple components as well.

Let's go through this step by step.

1. We have the whole file as it is.
`yourFileName.txt`
2. This file contains multiple **blocks**.
```
<block1>
<block2>
<block3>
...
<blockN>
```
3. Each of these blocks consist of a **head**- and a **config**-part.
```
<head> {
	<config>
}
```

So how does the head look like?
The head consists of a **type**-field and a **name**-field seperated by a "." (dot).
```
<type>.<name>
```
The type is mainly for readability $\rightarrow$ it is there so you know what kind of device you are working with here.
As you probably want to introduce new networks as well, you simply need to set the type to "Net"!
e.g.:
```
Net.foo {
	192.168.0.0/24
}
```
The program can handle IPv4 and IPv6 so feel free to go nuts!

If the type isn't explicitly "Net" then the **TopologyReader** will always treat it like any generic Device that has some sort of configuration in it.
e.g.:
```
AnythingButNet.bar {
	<config>
}
```

Alright, we covered the header so let's get to the config.
The config part is introduced by writing any config between two curly-brackets
```
{
	<config>
}
```
The config-part is always **after** the header of the device!
Inside the curly-brackets you then simply write generic network-configuration commands like usual.

These are the very basics and it isn't really any better than writing config in a generic txt-file, so what else can we do?

1. Assign multiple devices to the same config:
Here, we simply add up multiple **header**-parts in **one** block to say that "all these devices should recive the following config"
```
<header1>, <header2>, <header2> {
	<config for each device>
}
```
These headers can either be seperated by any amount of spaces (> 0) or any amount of newlines (> 0) or a combination of it if you are really brave.

2. Bind interfaces of devices to networks:
Don't you hate it when there are so many subnets and vlans and whatnot so that you always have to look through your topology to even know where you are? Then just bind the networks to the device's interfaces!
Doing so is very simple and straightforward:
```
<type>.<name>[
	<int1> in <network name 1>
	<int2> in <network name 2>
],
<otherheaders...> {
	<config>
}
```

Here's a list of supported interfaces:
- fastEthernet
- gigabitEthernet
- serial
- vlan (as this is technically an interface)

Luckily, all of these interfaces start with a different letter, so if you want to keep it short you just have to write the very first letter.
It also means that small typos (e.g.: fastEternetz) don't matter as long as the first letter is correct!

3. This syntax uses _placeholders_:
- $nth(number) - the nth address of the network
- $first - the first host-address of the network
- $last - the last host-address of the network
- $netaddr - the net-address of the network
- $broadaddr - the broad-address of the network
- $subaddr - the subnetmask of the network in DDN
- $subcidr - the subnetmask of the network in CIDR-Notation
- $wild - the wildcard-mask of the network in DDN 
- $name - the name of the device

You can use these placeholders in your config and when the program compiles the input-file it will replace the placeholders with there corresponding value depending on the device!

##### Example of an input file:
```
R.R1[
	fa0/0 in foo1
	gig0/1 in foo2
],
R.R2[
	fa0/0 in bar1
	gig0/1 in bar2
]{
	en
	conf t
	hostname $name
	motd #Logoff!!#
	int fa0/0
	ip address $first $subaddr
}

Net.foo1 {
	192.168.1.0/24
}

Net.bar1 {
	192.168.2.0/24
}

Net.foo2 {
	2001:CAFE:1::/64
}

Net.bar2 {
	2001:CAFE:2::/64
}
```

This input-file would produce the following outcomes:

**R1.txt**
```
en
conf t
hostname R1
motd #Logoff!!#
int fa0/0
ip address 192.168.1.1 255.255.255.0
```

**R2.txt**
```
en
conf t
hostname R2
motd #Logoff!!#
int fa0/0
ip address 192.168.2.1 255.255.255.0
```

## Input-File

If you have written your input-file and sent it through the program, it will produce the output-files in the following manner:
Each file is labled \<device-name>.txt with the full, translated configuration in it (copy-pasteable).

#### Device

**A device is anything that is not a network.**
No matter the Device, storing it is always the same:
- name - The name of the device
- config - The commands used for configuration

The `config`-variable is a simple String containing the fully assigned configuration.
Ensuring the correctness of the configuration itself is subject to the user itself. $\rightarrow$ it is not possible for the program to tell if you set the values correctly as all it knows is the information you provide. 

When first created, the `config`-variable will **not** contain the translated version of the configuration - it will later be translated within the **VariableConverter**-Class.

Therefore, the **Device**-Class is just used as a storage:
```java
public class Device {

	private final String name;
	private String config;

	public Device(String name, String config) {
		this.name = name;
		this.config = config;
	}

	public String getName() {
		return this.name;
	}

	public String getConfig() {
		return this.config;
	}

	public void setConfig(String config) {
        this.config = config;
    }

    @Override
    public String toString() {
        return String.format(
                "Device: {name: %s, config: %s}",
                this.getName(), this.getConfig()
        );
    }

}
```

#### Network

A network is only recognised in the input-file if the **type**-Field is named "Net".
E.g.: 
```
Net.foo {
	192.168.0.0/24
}
```

A Network-Field only expects one entry - the IPv4 or IPv6 address combined with the subnetmask in slash-notation $\rightarrow$ `255.255.255.0` for the subnetmask is **invalid**.

The **Network**-Class will make use of the [IPAddress](https://github.com/maltalex/ineter) Library in order to parse the input, making sure that the given address is valid and convert a given address to any other address of the given address-space.

The implementation of the **Network**-Class is the following:

```java
public class Network {
}
```
It has the following instance-variables and constants:
- name - The name of the network
- ip - The subnet
- broadcast - The broadcast-address of the subnet
- wildcard - The subnetmask inverted ($\rightarrow$ the wildcardmask)
- ADDRESS_DELIMITER (`"/"`) - Since an address (in the input) should always have the format `<ip>/<subnetmask>`, we can then use the delimiter to seperate these from one-another.

```java
private final String name;
private final IPSubnet ip;
private final IPAddress broadcast;
private final IPAddress wildcard;
private final static String ADDRESS_DELIMITER = "/";
```

In the constructor, we simply set the `name` and the subnet `ip` as well as the `broadcast`- and `wildcard`-address.

After the subnet is set, getting information like `broadcast` is very simple as it makes use of methods from the library.

As you can see, this is also the place where we make use of the `ADDRESS_DELIMITER`.
```java
public Network(String name, String fullAddress) {
	
	this.name = name;

	final String[] splittedAddress = fullAddress.split(ADDRESS_DELIMITER);
	
	int maskLen = Integer.parseInt(splittedAddress[1]);
	
	if(IPAddress.of(splittedAddress[0]).version() == 4) {
		this.ip = IPv4Subnet.of(splittedAddress[0], maskLen);
		this.wildcard = ((IPv4Subnet) ip).getNetworkMask().not();
		this.broadcast = ((IPv4Subnet) ip).getNetworkAddress().or((IPv4Address) wildcard);
	} else {
		this.ip = IPv6Subnet.of(splittedAddress[0], maskLen);
		this.wildcard = ((IPv6Subnet) ip).getNetworkMask().not();
		this.broadcast = ((IPv6Subnet) ip).getNetworkAddress().or((IPv6Address) wildcard);
	}

}
```

Although the `IPAddress`-class already has a `plus`-method, we need to "override" it in order to be able to:

1. Use a `String` as an input for convinience
2. Handle _potentially_ large numbers (larger than the long-datatype can store)

**Note:** This method always adds to the network-address and always returns a new `IPAddress` with the added value instead of overwriting the network-address.

To account for numbers larger than 2^64 (> `long`) we are going to use the `BigInteger`-class.

Then we simply add to the `BigInteger of n` with the network-address converted to a `BigInteger` as well. 

At the end we also check if the resulting address is in the range of the network-address and the broadcast-address as this is most-likely not wanted in practise. If it is not in range then we stop the program by throwing an Exception to notify the user.

**Note:** If the new IP is equal to the network-address or the broadcast-address then this is treated as **in** range and it will **not** throw an Exception.

```java

private IPAddress plus(String n) {
		
	IPAddress addedIP;

	try {

		BigInteger toAdd = new BigInteger(n);
		BigInteger netIP = new BigInteger(
			this.ip
				.getNetworkAddress()
				.toInetAddress()
				.getAddress()
		);

		addedIP = IPAddress.of(
				netIP
					.add(toAdd)
					.toByteArray()
		);

	} catch(NumberFormatException e) {
		throw new IllegalArgumentException("Input is not a number: " + n);
	}

	if(!isInRange(this.getNetworkAddress(), this.getBroadcastAddress(), addedIP)) {
		throw new IllegalArgumentException(String.format(
				"%s is not in range of %s - %s", addedIP, this.getNetworkAddress(), this.getBroadcastAddress()
		));
	}

	return addedIP;

}
```

The `isInRange`-method basically just makes use of the `ip.version`- and `ip.compareTo`-method.
Since the interface `IPAddress` itself does not have the `compareTo`-method we need to check the `version` of the ip (either IPv4 or IPv6) and then cast accordingly.

An `IPAddress` `isInRange` if the following boolean is true: `low <= ip && ip <= high`.
	
```java
private static boolean isInRange(IPAddress low, IPAddress high, IPAddress ip) {
	return isLargerOrEqualThan(ip, low) && isSmallerOrEqualThan(ip, high);
}

private static boolean isLargerOrEqualThan(IPAddress ip1, IPAddress ip2) {
	if(ip1.version() == 4 && ip2.version() == 4) {
		return ((IPv4Address) ip1).compareTo((IPv4Address) ip2) >= 0;
	}
	return ((IPv6Address) ip1).compareTo((IPv6Address) ip2) >= 0;
}

private static boolean isSmallerOrEqualThan(IPAddress ip1, IPAddress ip2) {
	if(ip1.version() == 4 && ip2.version() == 4) {
		return ((IPv4Address) ip1).compareTo((IPv4Address) ip2) <= 0;
	}
	return ((IPv6Address) ip1).compareTo((IPv6Address) ip2) <= 0;
}
```

And finally, the most complex methods:
### GETTERS
```java
public IPAddress getNetworkAddress() {
	return this.ip.getNetworkAddress();
}

public IPAddress getBroadcastAddress() {
	return this.broadcast;
}

public IPAddress getNthAddress(String n) {
	return this.plus(n);
}

public IPAddress getFirstHostAddress() {
	return this.getNetworkAddress().plus(1);
}

public IPAddress getLastHostAddress() {
	return this.getBroadCastAddress().minus(1);
}

public IPAddress getSubnetmaskAddress() {
	return this.ip.getNetworkMask();
}

public int getSubnetmaskCIDR() {
	return this.ip.getNetworkBitCount();
}

public IPAddress getWildcardAddress() {
	return this.wildcard;
}

public String getName() {
	return this.name;
}
```
... and a `toString` for good measure.

```java
@Override
public String toString() {
	return String.format(
		"Network: {name: %s, range: %s - %s}",
		this.name, this.networkAddress, this.broadcastAddress
	);
}
```
And just like that, we are now able to easily insert a network-address with a subnetmask and recieve any IP address in that address-space no-matter if it is IPv4 or IPv6. Noice.

### NetworkBinding
Now that we have the Network set-up, we still need a system to save the various **Bindings**:
It should be possible to go to a Device and bind an interface of that **Device** and bind it to a **Network**.
This class is very simple and just used as a storage, we need 3 variables to store the information of one NetworkBinding: The **device-name**, the **interface** of that device and the **network** that the interface is bound to.

The syntax for assigning an interface to a network goes like the following:
`<interface> in <network>`.
So we simply split this String at the "in" (with at least one space before and after).
The first index will then be the interface, the second the network. 

One feature that this class needs to have is translating the user-put interface-name to a consistent short version.
This is useful because we can then later check easier which binding should be used as every different type of interface (FastEthernet, GigabitEthernet, Serial, VLANs) starts with a different character anyway.

```java
public class NetworkBinding {

	private final String deviceName; 
	private final String interfaze;
	private final String networkName;
	private final static String INTERFAZE_NETWORK_DELIMITER = "\\s+in\\s+";

	public NetworkBinding(String deviceName, String binding) {
		this.deviceName = deviceName;
		String[] splittedBinding = binding.split(INTERFAZE_NETWORK_DELIMITER);
		this.interfaze = this.getShortVersion(splittedBinding[0]);
		this.networkName = splittedBinding[1];
	}

	private String getShortVersion(String interfaze) {

		final String[] shortVersions = {"f","g","s"};
		final String interfazeBeginning = interfaze.charAt(0) + "";
		final String interfazePort = interfaze.replaceFirst("^[^\\d]*", "");
		
		for(String shortVersion : shortVersions) {
			if(interfazeBeginning.equals(shortVersion)) {
				return shortVersion + interfazePort;
			}
		}

		throw new IllegalArgumentException("Interface-Type is unknown: " + interfaze);

	}

	public String getDeviceName() {
		return deviceName;
	}

	public String getInterfaze() {
		return interfaze;
	}

	public String getNetworkName() {
		return networkName;
	}

}
```

### TopologyReader 2: Electric Boogaloo

Now that we have set-up everything that is necessary to store and retrieve the wanted information, we can now start creating the class that actually takes the input-file as a String and creates the various `Devices` and `Networks` with it: the **`TopologyReader`**.

As stated, the `TopologyReader` will **not** take the input-file as a File or Path but as a String. This String will be created by the `InputManager` which will be the last class to be covered in the documentation.

```java
public class TopologyReader {
}
```

#### What does it do?
The main job of the TopologyReader is to know the syntax of the file and then therefore be able to:
1. Know how to seperate full blocks (header + config) 
2. Know how to distinguish a header-block from a config-block
3. Know that this "header" can contain multiple devices
4. Know how to distinguish a generic Device from a Network
5. Know that for **Devices** only - network-bindings are **possible** (but not mandatory!).
6. Know that the same devices can be used multiple times and therefore should not be created twice.

#### What can it _not_ do then?
The TopologyReader **cannot** differentiate generic text from a **placeholder**.
This task is left out for the VariableConverter to tackle.

So let's get started by simply ticking off the list one step after another.

###### 1. Seperate blocks
Just as a recap, this is how a header- and config-block looks like:

###### Header
(so it reads more easily)
let headerComponent = `<Device|Network>.<name>[port in network]`
```
headerComponent1 headerComponent2 headerComponent3...
```

###### Config
```
HEADER {
	literally anything
}
```

Alright, seems simple enough (especially the config part).

Since we now know that a full block (meaning header + config) always ends with a "}", we can start at the beginning of the file and everything up to the first "}" is a full block, and then we find the next "}" and that is the second full block. So on and so forth. We obviously only want these blocks already seperated, so we can do this in the constructor by default.

```java
public class TopologyReader{
	private final List<Network> networks = new ArrayList<>();
	private final List<Device> devices = new ArrayList<>();
	private final List<NetworkBinding> networkBindings = new ArrayList<>();
	
	private final static String BLOCK_DELIMITER = "\\}";
}
```

```java
public TopolyReader(String input) {
	String[] blocks = input.split(BLOCK_DELIMITER);
}
```
The way that the `String.split`-method works is that it seperates Strings at the given Character. The "by-product" of this is tho that we loose the specified Character. Luckily, this character is only a delimiter and not needed afterwards anyways. The only thing that we have to remember now is that we do not have to worry about it anymore anyways. Nice.

###### Seperate Header from Config
Not much to explain here: The header ends when the character "{" is reached, everything after that in a block is config.
```java
private final static String HEADCONF_DELIMITER = "\\{";
```

```java
String[] headconf = block.split(HEADCONF_DELIMITER);
String header = headconf[0];
String config = headconf[1];
```
Through the "by-product" of the `split`-method we now actually have the config exactly as we need it. Nice #2.

###### Seperate multiple Devices in the same header
Here, we decided to make a comma as a delimiter between multiple devices.
Therefore, the delimiter is very complex once again:
```java
private final static String DEVICES_DELIMITER = ",\\s*";
``` 
The `\\s*` is there so that we do not have to trim everything afterwards if the user decided to add spaces after the comma.

```java
String[] devices = header.split(DEVICES_DELIMITER);
```

###### Distinguish Device from Network
_Everything that isn't a Network is a Device._

A Network is identified if the `<Type>`-Field is called "Net".
The `<Type>`-Field ends when it reaches a "."(dot).
Easy enough! Let's implement it.

```java
private final static String TYPE_DELIMITER = "\\.";
private final static String NETWORK_STRING = "Net";
```

```java
String[] splittedType = device.split(TYPE_DELIMITER);
String type = splittedDevice[0];
String afterType = splittedDevice[1];

private boolean isNetwork(String device) {
	return device.equals(NETWORK_STRING);
}
```

Since we do not need the `<Type>`-Field for anything else anymore, we also create a variable `afterType` that has everything in the header after that field, so we do not have to worry about it anymore. Nice #3.

###### Name

Just to visualize it easier, this is how the `afterType`-String now looks like:
`<name>[bindings]`
Although the bindings (including the "[]") are optional.

So, we only have 2 parts of information left. So if we seperate one from the other, we ultimately extract the other as well.

```java
private final static String NAME_DELIMITER = "\\s*\\[";
```

```java
String[] splittedName = afterType.split(NAME_DELIMITER); 
String name = splittedName[0];

String bindings = "";
if (splittedName.length > 1) {
	bindings = splittedName[1];
}
```

###### Bindings

And there we go, now we simply want to seperate all the bindings from one-another if they exist. If a binding exists, we also want to remove the "]" at the end as we do not need it anymore. No need for a regex or a split here tho, as we can simply remove the last char of the bindings-String.

```java
private final static String BINDINGS_DELIMITER = "\\n+"; 
```

```java
List<String> seperatedBindings = new ArrayList<>();
if(!bindings.isBlank()) {
	String cleanBindings = bindings.substring(0, bindings.length() - 1);
	seperatedBindings = new ArrayList<>(Arrays.asList(cleanBindings.split(BINDINGS_DELIMITER)));
}
```

That is it folks!

Now that we have all the data, let's use it.

Here, a quick recap of all the data our mighty program posesses:
- List of blocks - Each block has
	- header - Each header has
		- type (network or device)
		- name
		- optional List of bindings (only possible for devices)
	- config (ip or actual config)

As seen above, there are three classes we can create: `Device`, `Network` and `NetworkBinding`.
So let's put in the data as necessary.

```java
if(isNetwork(type)) {
	
	networks.add(
		new Network(name, config)
	);

} else {

	devices.add(
		new Device(name, config)
	);

	seperatedBindings.forEach(networkBinding -> {
		networkBindings.add(new NetworkBinding(name, networkBinding))
	});
	
}
```

And just like that, we have the complete topology mapped out. Ultimate Nice.

## VariableConverter and Variables
Now that we have everything covered, we now simply have to replace any variable/placeholder with the expected value.

So we have to have: 
- a String that defines how a variable starts 
- a String that defines what the name of the variable is
- a String that replaces the variable.

We also need a system that allows for parameters.
Parameters will be put in like this: `$var(param)`.
An easy Regex for the param only would therefore look like this: `\((.*?)\)`, which simply says "take everything between two brackets as one String"
Params aren't always going to be needed tho, so we make this part optional:
`(?:\((.*?)\))?`

The good thing about this system is, that technically you can put in parameters into any variable and it will still work as every Variable removes these parameters wheter or not they actually need it.

For the sake of expandability, we create an enum `Variable` that stores exactly that.

The VariableConverter will always go through each Variable-type one by one and for each Variable-type replace each occurence from the top to bottom. This way we do not have to worry about weird Regex and simpyly take the first match, work with it and after it has been replaced, go to the next "first" match until everything has been converted.

Ultimately the full Regex-Pattern looks like this: `PREFIX + VARNAME + PARAMS`.

```java
this.variablePattern = Pattern.compile(PREFIX + this.name + PARAMS, Pattern.CASE_INSENSITIVE);
```

To be more user-friendly, we will also make the Variables case-*insensitive*.

We will simply return the compiled regex with a little one-liner method.
```java
public Matcher getVariableMatcher(String input) {
    return this.variablePattern.matcher(input);
}
```

The last question now is: "How do we get the correct replacement?".
The answer is quite simple: All the necessary information is available from the device itself and the position that the variable was found at.

We only have two different "types" of variables:
1. Variables that only need "static" information from the device/general (NAME)
	1.1. These are very easy to implement as we simply replace one String by another.
2. Variables that have a different replacement depending on the position they are at in the configuration. (any IPAddress-related variable)
	2.1. This is because we need to know which interface was last accessed so we can get the corresponding binding and then get the necessary IP-Address.

**Note:** Both of these types can have parameters, so we have to account for that at all times!

The 2nd type is a bit more tricky but nothing that we cannot manage. The importance here is, that we need to know *where* the variable is, so we can then backtrack from its position to the first occurence of the `interface <interface>` command. When we found that command, we then need to extract the `<interface>` from it. When this is done, it's basically just one method-call after another.

So, first of all, let's create a regex that filters this `<interface>`:
`int.*?\\s*(.*?)$`

It might seem a bit confusing, but let's look at what this regex does:
1. `int` $\rightarrow$ simply searches for the literal String "int"
2. `.*?` $\rightarrow$ since some might write the command like `int` others could write `inter` or maybe `interface`. This cannot be foreseen so we just look for the minimum `int` and then say "there also might be something after that before a space is reached"
3. `\\s+` $\rightarrow$ at least one space or tab, since that seperates the command from its parameter
4. `(.*?)` $\rightarrow$ that's the exciting one, this is the capture group that filters our interface.
5. `$` $\rightarrow$ After the argument, the line must end.

And that is already it, so let's implement this real quick:

```java
private final static String INTERFACE = "int.*?\\s+(.*?)$";
```

So now that we can find interface-commands, how do we get the correct one?
Luckily, whenever we find a variable through a matcher, we get index of that find as well.
Therefore, we can simply create a substring from the beginning of the conf-file up until that find and then search for the interfaces. Then we simply take the last one found and that is the one that we need. It will look a little like that:

```java

final Pattern interfacePattern = Pattern.compile(INTERFACE); 

while(varMatcher.find()) {
	if(variable.isInterfaceDependent()) {

		final int varPos = varMatcher.start();
		final String allInterfaces = device.getConf().substring(0, varPos);

		Matcher matcher = interfacePattern.matcher(allInterfaces);
		String interfaze = "";

		while(matcher.find()) {
			interfaze = matcher.group(1);
		}

	}
}
```

In the variable `interfaze` is now the last interface that has been found. Through the static method of the `NetworkBinding`-Class `getShortVersion` we can then convert this interface to a standardized form.

```java
String standardizedInterface = NetworkBinding.getShortVersion(interfaze);
```

Now we can create a method in the `TopologyReader` that returns the Network that the interface is bound to. For that, we need to provide the device and the interface - both of those we now have.

```java
public Network getNetworkByBinding(Device device, String interfaze) {
	for(NetworkBinding networkBinding : this.networkBindings) {

		if(
			networkBinding.getDeviceName().equals(device.getName()) && 
			networkBinding.getInterfaze().equals(interfaze)
		) {
			return this.getNetworkByName(networkBinding.getNetworkName());
		}

	}
}
```
Getting the Network by its name is very simpel:

```java
public Network getNetworkByName(String name) {
	for(Network network : this.networks) {
		if(network.getName().equals(name)) {
			return network;
		}
	}
}
```

The correct Network isn't the only "dynamic" information that might be needed tho; there are parameters as well.
Therefore we also need to filter out the provided Parameters for further usage.

```java
final String parameters = varMatcher.group(1);
```

Now we have access to the: Device, Network, NetworkBinding and the possible parameters for that one variable. There is literally no information left that we could possibly still ask for. Nice.

So, let's create this class `VariableConverter`.
Let's quickly recap what we have to go through:
1. Access each variable
2. Match every occurence of that variable **one by one**
3. Provide any necessary information to that variable. - This information can be one of two things:
3.1. Network 
3.2. Parameters
4. Let the variable create the replacement-value
5. Replace that placeholder with the replacement-value and save the new String.

Since we have two "dynamic" informations and one "static" information we will simply provide the "static" information (aka. the `Device`-Object) via a static Variable that we simply set in the enum `Variable` whenever the `getConvertedConfig`-method gets called and the "dynamic" information via arguments to the `getReplacementValue`-method of the `Variable`-enum.

For the sake of efficiency, each Variable automatically compiles the corresponding Pattern at the start of the program so we do not need to compile the Patterns again and again.

```java
public class VariableConverter {

	private final static Variable[] VARIABLES = Variable.values();
	private final TopologyReader topologyReader; 

	public VariableConverter(TopologyReader topologyReader) {
		this.topologyReader = topologyReader;
	}

	public String getConvertedConfig(Device device) {
		
		Variable.setDevice(device);

		String convertedConfig = device.getConfig();

		for(Variable variable : VARIABLES) {

			Matcher varMatcher = variable.getMatcher(convertedConfig);
			
			while(varMatcher.find()) {
				
				final String parameters = varMatcher.group(1);

				if(variable.isNetworkDependent()) {
					final int varPos = varMatcher.start();
					final String allInterfaces = device.getConf().substring(0, varPos);

					Matcher matcher = interfacePattern.matcher(allInterfaces);
					String interfaze = "";
					while(matcher.find()) {
						interfaze = matcher.group(1);
					}

					interfaze = NetworkBinding.getShortVersion(interfaze);
					final Network network = TopologyReader.getNetworkByBinding(device, interfaze);

					convertedConfig = varMatcher.replaceFirst(variable.getReplacement(parameters, network));

				}

				convertedConfig = varMatcher.replaceFirst(variable.getReplacement(parameters, null));

			}
		
		}

		return convertedConfig;

	}

}
```

And that's already it, now we just have to construct all the Variables that we want.
Since we are using up to two "dynamic" parameters, we will use the functional interface `BiFunction` as this one takes two parameters and puts out one result.
We therefore have to define three Datatypes for the `BiFunction`, these will be:
- `String` and `Network` as the parameters - The String contains the parameters that the variable takes in the text-file.
- `String` as the return-value

Let's get to it then!
Again, a quick recap of what we need:
1. A prefix that identifies the start of a variable
2. The name of the variable
3. A boolean that says if that variable needs information from a network
4. A BiFunction to replace the variable with the correct String

```java
public enum Variable {

	;

	private final static String PREFIX = "\\$";
	private final static String PARAMS = "(?:\\((.*?)\\))?";
	private final static Pattern INTERFACE_PATTERN = Pattern.compile("int.*?\\s+(.*?)$");
	private final String name;
	private final static Pattern VARIABLE_PATTERN;
	private final boolean networkDependent;
	private final BiFunction<String, Network, String> replaceFunction;

	private static Device device;

	Variable(String name, boolean networkDependent, BiFunction<String, Network, String> replaceFunction) {
		this.name = name;
		this.networkDependent = networkDependent;
		this.replaceFunction = replaceFunction;

		VARIABLE_PATTERN = Pattern.compile(PREFIX + this.name + PARAMS);

	}

	public Matcher getVariableMatcher(String input) {
		return VARIABLE_PATTERN.matcher(input);
	}

	public static Matcher getInterfaceMatcher(String input) {
        return INTERFACE_PATTERN.matcher(input);
    }

	public boolean isNetworkDependent() {
		return this.networkDependent;
	}

	public String getReplacement(String params, Network network) {

		return this.replaceFunction.apply(params, network);

	}

	public void setDevice(Device device_) {
		Variable.device = device_;
	}

	private static Device getDevice() {
        return device;
    }

}
```

And voilà, everything is set-up! Now let's get to the fun part and define some very useful variables!

```java
NTH("nth", true, (parameter, network) -> network.getNthAddress(parameter).toString()),
FIRST("first", true, (parameter, network) -> network.getFirstHostAddress().toString()),
LAST("last", true, (parameter, network) -> network.getLastHostAddress().toString()),
NETADDR("netaddr", true, (parameter, network) -> network.getNetworkAddress().toString()),
BROADADDR("broadaddr", true, (parameter, network) -> network.getBroadcastAddress().toString()),
SUBADDR("subaddr", true, (parameter, network) -> network.getSubnetmaskAddress().toString()),
SUBCIDR("subcidr", true, (parameter, network) -> network.getSubnetmaskCIDR() + ""),
WILD("wild", true, (parameter, network) -> network.getWildcardAddress().toString()),
NAME("name", false, (parameter, network) -> getDevice().getName());
```

Tada!

## IOManager
This is the last stretch!
The IOManager is very simpel, its only purpose is to manage these few, easy tasks:
- Take the input-file and convert it to a String
- Put that String into the TopologyReader
- Let the VariableConverter convert every Device
- Whenever the VariableConverter converted a Device, save the converted version and create a File with this converted version
- Put all the files nicely in the directory of choice

Here we go!

```java
public class IOManager {

	private final static Pattern EMPTY_LINES_AND_TABS = Pattern.compile("^\\s+", Pattern.MULTILINE);

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.println("Please enter the absolute Path to your input-file!");
        String input = in.nextLine();

        System.out.println("Please enter the absolute Path to your output-directory!");
        String outputDir = in.nextLine();

        TopologyReader tr = new TopologyReader(input);
        VariableConverter vc = new VariableConverter(tr);

        tr.getDevices().forEach(device -> {

            File deviceFile = new File(outputDir + "\\" + device.getName() + ".txt");
            String deviceConf = vc.getConvertedConfig(device);
            deviceConf = EMPTY_LINES_AND_TABS.matcher(deviceConf).replaceAll("");
            try {
                Files.createFile(deviceFile.toPath());
            } catch (IOException e) {
                throw new IllegalArgumentException("Could not create file with the path: " + deviceFile.getAbsolutePath());
            }

            try {
                Files.writeString(deviceFile.toPath(), deviceConf);
            } catch (IOException e) {
                throw new IllegalArgumentException("Could not write to file with the path: " + deviceFile.getAbsolutePath());
            }

        });


    }

}
```

# THIS
# IS
# IT!

or is it?

Technically, we are done now. You as a user can now put the input-file into the program and it will very nicely create all the necessary config files for you.

Therefore - this is now a full version: **Version 1.0**

Everything added after this are extras, so what's to come?

---------

# EXTRAS - YAY!

Jeez, so you want some more stuff? Alright, here's what you can get:

- Split-Up device config (configuring the same device at different "places")
- Simple GUI (just stop arguing that it is less secure, nobody cares...)

I think this is good enough for now. Whenever one item is ticked off the list, the version will be updated like this `v1.1`, `v1.2`, etc.

### Split Config

Split Config simply means that you can create multiple device-blocks to split up configuration.
This can be used to for example put general config to every device and then more specific config seperated.

In code, this means that as we get the list of all the devices, we simply check if the current (just found) device already exists.
If it doesn't $\rightarrow$ procede as usual
If it does $\rightarrow$ take the config of the current device and append it to the already existing device. We therefore do not need to create a new Device-Object.

Let's implement it then!

**Note:** Placeholders only get replaced after the full topology has been read, therefore we do not need to redefine bindings in split configuration. Technically this is possible but it is definitely not needed and won't be benefitial in any way.  

```java
// (in TopologyReader)

if(isNetwork(type)) {
	// nothing to change here
} else {
	
	Device originalDevice = this.getOriginalDevice(name);

	if(originalDevice != null) {
		originalDevice.setConfig(originalDevice.getConfig() + "\n" + config.replaceAll("^[\\r\\n]+", ""));

	} else {
		// add new Device as usual
	}

}

```

There we go, very simple and very effective!


### Simple GUI

Alright, so before we can get to actually making the GUI, we first need to know what we want to be able to do in that GUI.
Let's see:
- Choose input-file and output-directory
- Help creating input-file without needing to worry about syntax

As the GUI will be created with JavaFX, doing the first item of the List is very easy.
All we need is a FileChooser and a DirectoryChooser for the input and output respectively.

In order to get the Path-Object of the file we will use the following snippet:

```java
String inputFilePath;

final FileChooser fileChooser = new FileChooser();
fileChooser.setTitle("Choose Input-File");

inputFilePath = fileChooser.showOpenDialog(stage).getAbsolutePath();
inputTextField.setText(inputFilePath);
```

And that is it! Choosing the output-directory is basically just the same:

```java
String outputDirectoryPath;

final DirectoryChooser directoryChooser = new DirectoryChooser();
directoryChooser.setTitle("Choose Output-Directory");

outputDirectoryPath = directoryChooser.showDialog(stage).getAbsolutePath();
outputTextField.setText(outputDirectoryPath);
```

Now that we have got the functionality programmed in, we simply have to assign it to JavaFX-Buttons to trigger these code-snippets on a button-press.

```java
public class Controller {

	@FXML
	public TextField inputTextField, outputTextField;

	@FXML
	public void putInputFilePath() {
		String inputFilePath;

        final FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose Input-File");

        try {
            inputFilePath = fileChooser.showOpenDialog(getStage()).getAbsolutePath();
            inputTextField.setText(inputFilePath);
        } catch (NullPointerException ex) {
            // ignore
        }
	}

	@FXML
	public void putOutputDirectoryPath() {
		String outputDirectoryPath;

        final DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Choose Output-Directory");

        try {
            outputDirectoryPath = directoryChooser.showDialog(getStage()).getAbsolutePath();
            outputTextField.setText(outputDirectoryPath);
        } catch (NullPointerException ex) {
            // ignore
        }
	}

	private Stage getStage() {
		return (Stage) inputTextField.getScene().getWindow();
	}
	
}
```

This addition is the very basic of the GUI-System. Meaning that we can already try it out. First of all we still need a "Run"-Button to let our generator compile the input-file.

This is very simple as before our program executed from one main-method as well. Therefore we simply say:
On Button-Press -> run IOManager.main();

Since main takes a String[] as arguments, we can simply put the input-file in the as the first index and the output-dir as the second index.

```java
@FXML
public void compile() {
	IOManager.main(new String[]{inputTextField.getText(), outputTextField.getText()});
}
```

And now we simply bind it to a Button again in SceneBuilder.

We also said that we want the GUI to help the user to create these input-files without needing to worry about syntax.
So let's go through everything again that our program covers:
1. Devices
	1.1 Config
	1.2 NetworkBindings
2. Networks
3. Compiling / Creating

(4. Loading)

So we can make three different tabs/windows, one for each of these "Classes" (Devices and Networks and Compiling).
To begin with, let's make a quick sketch so we know what we want:
![](./sketches/confgenerator_gui_sketch.png)

We currently simply want to create config files. It makes more sense to start working on loading a config file after we manage to actually compile with the gui since this needs to be done after loading as well.

Alright, let's build this real quick.

![](./sketches/gui_preview.png)

Seems good enough for now (I am not an artist so thank god that this is open-source)

To begin with, let's simply make it possible to add networks and devices to the corresponding lists without any further "meaning" behind it.

Since we use a `TableView` in the Networks- and Bindings-tab we need to make a container-class to add rows to it:

```java
package gui;

public class NetworkInput {

    private final String name, ip, sub;

    public NetworkInput(String name, String ip, String sub) {
        this.name = name;
        this.ip = ip;
        this.sub = sub;
    }

    public String getName() {
        return name;
    }

    public String getIp() {
        return ip;
    }

    public String getSub() {
        return sub;
    }

}
```

```java
package gui;

public class BindingInput {

    private final String device, network, interfaze;

    public BindingInput(String device, String network, String interfaze) {
        this.device = device;
        this.network = network;
        this.interfaze = interfaze;
    }

    public String getDevice() {
        return device;
    }

    public String getNetwork() {
        return network;
    }

    public String getInterfaze() {
        return interfaze;
    }

}
```

We also need to use this little snippet to let the table know which variable-value goes into which column and to make the List-/Table-Items multi-selectable:

```java
@FXML
public void initialize() {
	devicesView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
	networksTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
	bindingsTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

	networkNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
	networkIPColumn.setCellValueFactory(new PropertyValueFactory<>("ip"));
	networkSubColumn.setCellValueFactory(new PropertyValueFactory<>("sub"));

	bindingDeviceColumn.setCellValueFactory(new PropertyValueFactory<>("devices"));
	bindingNetworkColumn.setCellValueFactory(new PropertyValueFactory<>("network"));
	bindingInterfaceColumn.setCellValueFactory(new PropertyValueFactory<>("interfaze"));
}
```

The basic implementation is very straightforward then:

```java
@FXML
public void addNetworkToList() {
	final String name = networkNameTF.getText(),
					ip = ipTF.getText(),
					sub = subTF.getText();
	networksTable.getItems().add(new NetworkInput(name, ip, sub));
}
```

So what could possible go wrong with this? Actually, not that much but it would be nice to
- not allow empty input (as this would not make sense)
- not allow duplicate input (which wouldn't make sense as well)

```java
@FXML
public void addNetworkToList() {
	final String name = networkNameTF.getText().trim(),
					ip = ipTF.getText().trim(),
					sub = subTF.getText().trim();

	if(name.isEmpty() || ip.isEmpty() || sub.isEmpty()) {
		networkUserErrorLabel.setText(EMPTY_FIELD_ERR_MSG);
		networkUserErrorLabel.setVisible(true);
		return;
	}

	for(NetworkInput networkInput : networksTable.getItems()) {
		if(networkInput.name.equals(name)) {
			networkUserErrorLabel.setText(DUPLICATE_ENTRY_ERR_MSG);
			networkUserErrorLabel.setVisible(true);
			return;
		}
	}

	networkUserErrorLabel.setVisible(false);
	networksTable.getItems().add(new NetworkInput(name, ip, sub));
}
```

And there we go!
Now let's go over to the Devices-Tab.

Adding a device to the view is basically just copy-pasting from the method above:

```java
 @FXML
public void addDeviceToList() {

	String name = deviceNameTF.getText().trim();

	if(name.isEmpty()) {
		deviceUserErrorLabel.setText(EMPTY_FIELD_ERR_MSG);
		deviceUserErrorLabel.setVisible(true);
		return;
	}
	
	for(String deviceName : devicesView.getItems()) {
		if(deviceName.equals(name)) {
			deviceUserErrorLabel.setText(DUPLICATE_ENTRY_ERR_MSG);
			deviceUserErrorLabel.setVisible(true);
			return;
		}
	}

	deviceUserErrorLabel.setVisible(false);
	devicesView.getItems().add(name);

}
```

Rinse and repeat for the Bindings-Tab:
```java
@FXML
public void addBindingToList() {

	final String device = devicesBindingsTF.getText().trim(),
					network = networksBindingsTF.getText().trim(),
					interfaze = interfacesBindingsTF.getText().trim();

	if(device.isEmpty() || network.isEmpty() || interfaze.isEmpty()) {
		bindingsUserErrorLabel.setText(EMPTY_FIELD_ERR_MSG);
		bindingsUserErrorLabel.setVisible(true);
		return;
	}

	for(BindingInput bindingInput : bindingsTable.getItems()) {
		if(bindingInput.getDevice().equals(device)) {
			bindingsUserErrorLabel.setText(DUPLICATE_ENTRY_ERR_MSG);
			bindingsUserErrorLabel.setVisible(true);
			return;
		}
	}

	bindingsUserErrorLabel.setVisible(false);
	bindingsTable.getItems().add(new BindingInput(device, network, interfaze));

}

@FXML
public void removeBindingFromList() {
	ObservableList<BindingInput> selectedBindings = bindingsTable.getSelectionModel().getSelectedItems();
	bindingsTable.getItems().removeAll(selectedBindings);
}
```

Even though it would be possible to DRY this up a bit I think that the resulting helper-methods would be more confusing than helpful (at least for now).

Now, let's do the opposite and create two methods to remove networks and device.

Luckily, this is very simple using the selection-model:

```java
@FXML
public void removeDevicesFromList() {
	ObservableList<String> selectedDevices = devicesView.getSelectionModel().getSelectedItems();
	devicesView.getItems().removeAll(selectedDevices);
}

@FXML
public void removeNetworksFromList() {
	ObservableList<NetworkInput> selectedNetworks = networksTable.getSelectionModel().getSelectedItems();
	networksTable.getItems().removeAll(selectedNetworks);
}
```

Two sub-tabs to go: Config and Bindings
Since these only make sense in the context of the selected devices we need an input-field to enter the names of the devices and preverably a dropdown-box from which one can choose the devices so you don't have to always double-check if you wrote the name correctly.

Therefore, we need to set the items of the dropdown-box equal to the items of the deviceView. This will be needed three times in total (once in the config-tab, twice in the bindings-tab). So let's quickly do this for every one of those dropdowns.

First, two helper-methods:
```java
private MenuItem createMenuItemOnAdd(String name, TextField textField, boolean isListing) {
	MenuItem menuItem = new MenuItem(name);
	menuItem.setOnAction(e -> {
		if (textField.getText().trim().isEmpty() || !isListing) {
			textField.setText(name);
		} else {
			textField.setText(textField.getText() + ", " + name);
		}
	});
	return menuItem;
}

private void removeMenuItemsOnRemove(List<String> removeds, MenuButton menuButton) {
	removeds.forEach(removed -> menuButton.getItems().removeIf(menuItem -> menuItem.getText().equals(removed)));
}
```

And now we just use these in our methods we just did. The important part here is that we remove an item from the menu-button **before** we remove it from the list since after an item has been removed from the list, the "focused-item" changes which therefore changes the values in the selection-model and would therefore remove the incorrect item from the menu-button. A bit weird, but it is what it is:
```java
@FXML
public void addDeviceToList() {
	...
	MenuItem menuItemConfig = createMenuItemOnAdd(name, devicesConfigTF, true);
	devicesConfigMB.getItems().add(menuItemConfig);

	MenuItem menuItemBinding = createMenuItemOnAdd(name, devicesBindingsTF, true);
	devicesBindingsMB.getItems().add(menuItemBinding);
}

@FXML
public void removeDevicesFromList() {
	...
	removeMenuItemsOnRemove(selectedDevices, devicesConfigMB);
	removeMenuItemsOnRemove(selectedDevices, devicesBindingsMB);
	// list removal ...
}

@FXML
public void addNetworkToList() {
	...
	MenuItem menuItemBinding = createMenuItemOnAdd(name, networksBindingsTF, false);
    networksBindingsMB.getItems().add(menuItemBinding);
}

@FXML
public void removeNetworksFromList() {
	List<String> names = selectedNetworks
			.stream()
			.map(NetworkInput::getName)
			.collect(Collectors.toList());
    removeMenuItemsOnRemove(names, networksBindingsMB);    
	// list removal ...
}
```

One more thing we have to do is remove these unnessecary template-items from all menu-buttons when we start the GUI.

```java
@FXML
public void initialize() {
	...
	devicesConfigMB.getItems().clear();
	devicesBindingsMB.getItems().clear();
	networksBindingsMB.getItems().clear();
}
```

And finally, we also do not want to list the same device multiple times, so we need to change the `menuItem.setOnAction` part a little:
```java
menuItem.setOnAction(e -> {
	if (textField.getText().trim().isEmpty() || !isListing) {
		textField.setText(name);
	} else {
		List<String> alreadyExisting = Arrays.stream(textField.getText().split(","))
				.filter(str -> !str.isEmpty())
				.map(String::trim)
				.collect(Collectors.toList());
		if(alreadyExisting.contains(name)) return;
		textField.setText(textField.getText() + ", " + name);
	}
});
```

Alright, now all we need is the possibility to add/update and remove config from a certain set of selected devices.
For this we will use a map where the device-combination is the key and the corresponding config is the value.
The map will only be accessed/updated whenever we hit the Add/Update button and whenever the textfield containing the list of devices changes. Currently, there is no remove-button as simply hitting ctrl + a, then deleting everything and then updating that acts the same and is basically just as fast.

Anyways, this is the map:
```java
private final Map<List<String>, String> devicesToConfigMap = new HashMap<>();
```
... where the `List<String>` represents the devices and `String` the config.

So, in order to update the map we simply fetch the values and put them into the map:
```java
@FXML
public void updateConfig() {
	final List<String> devices = Arrays.stream(devicesConfigTF.getText().split(","))
			.map(String::trim)
			.filter(device -> !device.isEmpty())
			.sorted()
			.collect(Collectors.toList());

	final String config = configTA.getText().trim();

	if(config.isEmpty()) devicesToConfigMap.remove(devices);
	else devicesToConfigMap.put(devices, config);
}
```

Similarly for loading the config:
```java
@FXML
public void loadConfig() {
	final List<String> devices = Arrays.stream(devicesConfigTF.getText().split(","))
			.map(String::trim)
			.sorted()
			.collect(Collectors.toList());

	configTA.setText(devicesToConfigMap.getOrDefault(devices, ""));
}
```

There we go! Now we can fully set-up all the information that we need to create an input-file with our GUI. The last thing we now need to do is create a method that takes all of this information, "applies" the syntax on it and then creates an input-file with it.

So, let's get to work! We can split up our "crafting" into three parts: NetworkSyntax, DeviceSyntax and BindingsSyntax!
The syntax for a network is very simple and easy to implement:
```java
private StringBuilder craftNetworkSyntaxString() {
	StringBuilder networksString = new StringBuilder();
	final String networkIndicator = "Net";
	networksTable.getItems().forEach(network -> {
		String networkString = String.format(
				"%s.%s {\n\t%s/%s\n}",
				networkIndicator, network.getName(),
				network.getIp(), network.getSub()
		);
		networksString.append(networkString).append("\n\n");
	});
	return networksString;
}
```

The device-syntax is a bit more complicated but nothing we can't handle. We simply take the list of devices for each entry and craft the "listing-String" with it. The rest is basically the same.
```java
private StringBuilder craftDeviceSyntaxString() {
	StringBuilder devicesString = new StringBuilder();
	final String deviceIndicator = "Dev";
	devicesToConfigMap.forEach((devices, config) -> {
		StringBuilder devicesFormatted = new StringBuilder();
		devices.forEach(device ->
				devicesFormatted.append(deviceIndicator).append(".").append(device).append(", ")
		);
		devicesFormatted.replace(devicesFormatted.length() - 2, devicesFormatted.length(), "");

		String deviceString = String.format(
				"%s {\n%s\n}",
				devicesFormatted, config.replaceAll("(?m)^", "\t")
		);
		devicesString.append(deviceString).append("\n\n");
	});
	return devicesString;
}
```

Luckily, since our IOManager doesn't care about ordering or something like that we can bundle the bindings all up to one place where we simply have an empty config afterwards. Not necessarily the structure that someone would do whilst writing the input-file by themselves but since the gui doesn't know where it should put the bindings otherwise we simply put them all in one place so it is easy to find at least.
```java
private StringBuilder craftBindingsSyntaxString() {
	StringBuilder bindingsString = new StringBuilder();
	final String deviceIndicator = "Dev";
	bindingsTable.getItems().forEach(binding -> {
		String bindingString = String.format(
				"%s.%s[\n\t%s in %s\n] { }",
				deviceIndicator, binding.getDevice(),
				binding.getInterfaze(), binding.getNetwork()
		);
		bindingsString.append(bindingString).append("\n\n");
	});
	return bindingsString;
}
```

And there we go! This is already everything we need, now we simply add these three Strings (or rather, StringBuilders) together and write that to the file of choice! 