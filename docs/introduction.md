# What is the ConfGenerator

Since I think that it is inefficient and error-prone to manually set-up a conf-file for each networking-device everytime (even if it just means to set new values for each device) I decided to make a program to help you with that.