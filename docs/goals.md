# What are the goals of this project?

### 1. Efficiency
By using this program you should be faster than if you would write and edit conf files the "original" way.

### 2. Consistency
By using this program you should not have to worry about changing "variables" between device anymore.

### 3. Useability
By using this program you should be able to get the conf-files quickly and change them easily if a new topology is given or the current topology changes.

### 4. User-friendly-design
By using this program you should know why and when the given input is incorrect or insufficient and what might need to change.