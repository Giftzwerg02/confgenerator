# 1 What should the code look like?

### 1.1 Variables
#### 1.1.1 Instance variables and method variables:
... will be written in camel-case: 

- `int thisIsMyVariable = 5`

A non-static constant and static non-constants will also be written like this:

- `final int thisIsMyVariable = 5`
- `static int thisIsMyVariable = 5` 

#### 1.1.2 Static Constants
... will be written in caps-only with underscores as a word-divider: 

- `final static int THIS_IS_MY_VARIABLE = 5`


### 1.2 Methods
Any method will be written in camel-case and consists of a verb and a noun:

- `doSomething() {...}`

### 1.3 Classes
#### 1.3.1 General
A "normal" class (meaning it does not match any of the exceptions below) will be a noun and written with the first letter capitalized.

- `Apple`
- `Computer`

#### 1.3.2 Abstract classes
An abstract class will be written just like a normal class but with an "A_" at the beginning of the name, signaling that it is an "A"bstract class.

- `A_Apple`
- `A_Computer`

#### 1.3.3 Interfaces
An interface will be written just like a normal class but with an "I_" at the beginning of the name, signaling that it is an "I"nterface

- `I_Apple`
- `I_Computer`

---

# 2 What should the code *not* look like?

For the sake of simplicity, the following "patterns" will not be of use in  the codebase:

- **self-made Exception classes**
- **Annotations**
