package gui;

public class BindingInput {

    private final String device, network, interfaze;

    public BindingInput(String device, String network, String interfaze) {
        this.device = device;
        this.network = network;
        this.interfaze = interfaze;
    }

    public String getDevice() {
        return device;
    }

    public String getNetwork() {
        return network;
    }

    public String getInterfaze() {
        return interfaze;
    }

}
