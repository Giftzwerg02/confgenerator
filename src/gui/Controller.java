package gui;

import conf_generator.IOManager;
import conf_generator.TopologyReader;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.paint.Color;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class Controller {

    @FXML
    public TextField inputTextField, outputTextField, devicesConfigTF, devicesBindingsTF, networksBindingsTF, interfacesBindingsTF;

    @FXML
    public TableView<NetworkInput> networksTable;

    @FXML
    public TableColumn<NetworkInput, String> networkNameColumn, networkIPColumn, networkSubColumn;

    @FXML
    public TableView<BindingInput> bindingsTable;

    @FXML
    public TableColumn<BindingInput, String> bindingDeviceColumn, bindingNetworkColumn, bindingInterfaceColumn;

    @FXML
    public ListView<String> devicesView;

    @FXML
    public TextField networkNameTF, ipTF, subTF, deviceNameTF;

    @FXML
    public TextArea configTA;

    @FXML
    public Label networkUserErrorLabel, deviceUserErrorLabel, configUserErrorLabel, bindingsUserErrorLabel,
            craftUserErrorLabel;

    @FXML
    public MenuButton devicesConfigMB, devicesBindingsMB, networksBindingsMB;

    private final Map<List<String>, String> devicesToConfigMap = new HashMap<>();

    private final static String EMPTY_FIELD_ERR_MSG = "Please fill out all fields!";
    private final static String DUPLICATE_ENTRY_ERR_MSG = "This entry already exists!";

    @FXML
    public void initialize() {
        devicesView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        networksTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        bindingsTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        networkNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        networkIPColumn.setCellValueFactory(new PropertyValueFactory<>("ip"));
        networkSubColumn.setCellValueFactory(new PropertyValueFactory<>("sub"));

        bindingDeviceColumn.setCellValueFactory(new PropertyValueFactory<>("device"));
        bindingNetworkColumn.setCellValueFactory(new PropertyValueFactory<>("network"));
        bindingInterfaceColumn.setCellValueFactory(new PropertyValueFactory<>("interfaze"));

        devicesConfigMB.getItems().clear();
        devicesBindingsMB.getItems().clear();
        networksBindingsMB.getItems().clear();
    }

    @FXML
    public void putInputFilePath() {
        String inputFilePath;

        final FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose Input-File");

        try {
            inputFilePath = fileChooser.showOpenDialog(getStage()).getAbsolutePath();
            inputTextField.setText(inputFilePath);
        } catch (NullPointerException ex) {
            // ignore
        }
    }

    @FXML
    public void putOutputDirectoryPath() {
        String outputDirectoryPath;

        final DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Choose Output-Directory");

        try {
            outputDirectoryPath = directoryChooser.showDialog(getStage()).getAbsolutePath();
            outputTextField.setText(outputDirectoryPath);
        } catch (NullPointerException ex) {
            // ignore
        }
    }

    private MenuItem createMenuItemOnAdd(String name, TextField textField, boolean isListing) {
        MenuItem menuItem = new MenuItem(name);
        menuItem.setOnAction(e -> {
            if (textField.getText().trim().isEmpty() || !isListing) {
                textField.setText(name);
            } else {
                List<String> alreadyExisting = Arrays.stream(textField.getText().split(","))
                        .filter(str -> !str.isEmpty())
                        .map(String::trim)
                        .collect(Collectors.toList());
                if(alreadyExisting.contains(name)) return;
                textField.setText(textField.getText() + ", " + name);
            }
        });
        return menuItem;
    }

    private void removeMenuItemsOnRemove(List<String> removeds, MenuButton menuButton) {
        removeds.forEach(removed -> menuButton.getItems().removeIf(menuItem -> menuItem.getText().equals(removed)));
    }

    @FXML
    public void addDeviceToList() {

        final String name = deviceNameTF.getText().trim();

        if(name.isEmpty()) {
            deviceUserErrorLabel.setText(EMPTY_FIELD_ERR_MSG);
            deviceUserErrorLabel.setVisible(true);
            return;
        }

        for(String deviceName : devicesView.getItems()) {
            if(deviceName.equals(name)) {
                deviceUserErrorLabel.setText(DUPLICATE_ENTRY_ERR_MSG);
                deviceUserErrorLabel.setVisible(true);
                return;
            }
        }

        deviceUserErrorLabel.setVisible(false);
        devicesView.getItems().add(name);

        MenuItem menuItemConfig = createMenuItemOnAdd(name, devicesConfigTF, true);
        devicesConfigMB.getItems().add(menuItemConfig);

        MenuItem menuItemBinding = createMenuItemOnAdd(name, devicesBindingsTF, true);
        devicesBindingsMB.getItems().add(menuItemBinding);

    }

    @FXML
    public void removeDevicesFromList() {
        ObservableList<String> selectedDevices = devicesView.getSelectionModel().getSelectedItems();
        removeMenuItemsOnRemove(selectedDevices, devicesConfigMB);
        removeMenuItemsOnRemove(selectedDevices, devicesBindingsMB);
        devicesView.getItems().removeAll(selectedDevices);
    }

    @FXML
    public void addNetworkToList() {
        final String name = networkNameTF.getText().trim(),
                     ip = ipTF.getText().trim(),
                     sub = subTF.getText().trim();

        if(name.isEmpty() || ip.isEmpty() || sub.isEmpty()) {
            networkUserErrorLabel.setText(EMPTY_FIELD_ERR_MSG);
            networkUserErrorLabel.setVisible(true);
            return;
        }

        for(NetworkInput networkInput : networksTable.getItems()) {
            if(networkInput.getName().equals(name)) {
                networkUserErrorLabel.setText(DUPLICATE_ENTRY_ERR_MSG);
                networkUserErrorLabel.setVisible(true);
                return;
            }
        }

        networkUserErrorLabel.setVisible(false);
        networksTable.getItems().add(new NetworkInput(name, ip, sub));

        MenuItem menuItemBinding = createMenuItemOnAdd(name, networksBindingsTF, false);
        networksBindingsMB.getItems().add(menuItemBinding);

    }

    @FXML
    public void removeNetworksFromList() {
        ObservableList<NetworkInput> selectedNetworks = networksTable.getSelectionModel().getSelectedItems();
        List<String> names = selectedNetworks
                .stream()
                .map(NetworkInput::getName)
                .collect(Collectors.toList());
        removeMenuItemsOnRemove(names, networksBindingsMB);
        networksTable.getItems().removeAll(selectedNetworks);
    }

    @FXML
    public void addBindingToList() {

        final String device = devicesBindingsTF.getText().trim(),
                     network = networksBindingsTF.getText().trim(),
                     interfaze = interfacesBindingsTF.getText().trim();

        if(device.isEmpty() || network.isEmpty() || interfaze.isEmpty()) {
            bindingsUserErrorLabel.setText(EMPTY_FIELD_ERR_MSG);
            bindingsUserErrorLabel.setVisible(true);
            return;
        }

        for(BindingInput bindingInput : bindingsTable.getItems()) {
            if(bindingInput.getDevice().equals(device)) {
                bindingsUserErrorLabel.setText(DUPLICATE_ENTRY_ERR_MSG);
                bindingsUserErrorLabel.setVisible(true);
                return;
            }
        }

        bindingsUserErrorLabel.setVisible(false);
        bindingsTable.getItems().add(new BindingInput(device, network, interfaze));

    }

    @FXML
    public void removeBindingFromList() {
        ObservableList<BindingInput> selectedBindings = bindingsTable.getSelectionModel().getSelectedItems();
        bindingsTable.getItems().removeAll(selectedBindings);
    }

    @FXML
    public void updateConfig() {
        final List<String> devices = Arrays.stream(devicesConfigTF.getText().split(","))
                .map(String::trim)
                .filter(device -> !device.isEmpty())
                .sorted()
                .collect(Collectors.toList());

        if(devices.isEmpty()) {
            configUserErrorLabel.setVisible(true);
            return;
        }

        configUserErrorLabel.setVisible(false);

        final String config = configTA.getText().trim();

        if(config.isEmpty()) devicesToConfigMap.remove(devices);
        else devicesToConfigMap.put(devices, config);
    }

    @FXML
    public void loadConfig() {
        final List<String> devices = Arrays.stream(devicesConfigTF.getText().split(","))
                .map(String::trim)
                .sorted()
                .collect(Collectors.toList());

        configTA.setText(devicesToConfigMap.getOrDefault(devices, ""));
    }

    @FXML
    public void compile() {
        try {
            IOManager.main(new String[]{inputTextField.getText(), outputTextField.getText()});
            craftUserErrorLabel.setTextFill(Color.GREEN);
            craftUserErrorLabel.setText("Compiling complete!");
        } catch (Exception e) {
            craftUserErrorLabel.setTextFill(Color.RED);
            craftUserErrorLabel.setText("Could not compile!");
        }


    }

    @FXML
    public void craft() {
        Path inputFilePath = Paths.get(inputTextField.getText());

        StringBuilder networks = craftNetworkSyntaxString(),
                      bindings = craftBindingsSyntaxString(),
                      devices = craftDeviceSyntaxString();

        String fullConfig = networks.append("\n").append(bindings).append("\n").append(devices).toString();

        try {
            if(!inputFilePath.toFile().exists()) {
                Files.createFile(inputFilePath);
            }
            Files.write(inputFilePath, fullConfig.getBytes());
        } catch (IOException e) {
            craftUserErrorLabel.setTextFill(Color.RED);
            craftUserErrorLabel.setText("Could not create config!");
            return;
        }

        craftUserErrorLabel.setTextFill(Color.GREEN);
        craftUserErrorLabel.setText("Crafting complete!");

    }

    @FXML
    public void craftAndCompile() {
        craft();
        compile();
    }

    @FXML
    public void loadInput() {
        putInputFilePath();
        Path inputFilePath = Paths.get(inputTextField.getText());
        try {
            String input = new String(Files.readAllBytes(inputFilePath), StandardCharsets.UTF_8);
            TopologyReader tr = new TopologyReader(input);

            networksTable.getItems().clear();
            tr.getNetworks().forEach(network -> {
                networkNameTF.setText(network.getName());
                ipTF.setText(network.getNetworkAddress().toString());
                subTF.setText(network.getSubnetmaskCIDR() + "");
                addNetworkToList();
            });

            devicesView.getItems().clear();
            devicesToConfigMap.clear();
            tr.getDevices().forEach(device -> {
                devicesToConfigMap.put(
                        Collections.singletonList(device.getName()),
                        device.getConfig().replaceAll("(?m)^[\t\n]",     "")
                );
                deviceNameTF.setText(device.getName());
                addDeviceToList();
            });

            bindingsTable.getItems().clear();
            tr.getNetworkBindings().forEach(binding -> {
                devicesBindingsTF.setText(binding.getDeviceName());
                interfacesBindingsTF.setText(binding.getInterfaze());
                networksBindingsTF.setText(binding.getNetworkName());
                addBindingToList();
            });
        } catch (IOException e) {
            throw new IllegalArgumentException("Couldn't read the file with the path: " + inputFilePath);
        }


    }

    private StringBuilder craftNetworkSyntaxString() {
        StringBuilder networksString = new StringBuilder();
        final String networkIndicator = "Net";
        networksTable.getItems().forEach(network -> {
            String networkString = String.format(
                    "%s.%s {\n\t%s/%s\n}",
                    networkIndicator, network.getName(),
                    network.getIp(), network.getSub()
            );
            networksString.append(networkString).append("\n\n");
        });
        return networksString;
    }

    private StringBuilder craftDeviceSyntaxString() {
        StringBuilder devicesString = new StringBuilder();
        final String deviceIndicator = "Dev";
        devicesToConfigMap.forEach((devices, config) -> {
            StringBuilder devicesFormatted = new StringBuilder();
            devices.forEach(device ->
                    devicesFormatted.append(deviceIndicator).append(".").append(device).append(", ")
            );
            devicesFormatted.replace(devicesFormatted.length() - 2, devicesFormatted.length(), "");

            String deviceString = String.format(
                    "%s {\n%s\n}",
                   devicesFormatted, config.replaceAll("(?m)^", "\t")
            );
            devicesString.append(deviceString).append("\n\n");
        });
        return devicesString;
    }

    private StringBuilder craftBindingsSyntaxString() {
        StringBuilder bindingsString = new StringBuilder();
        final String deviceIndicator = "Dev";
        bindingsTable.getItems().forEach(binding -> {
            String bindingString = String.format(
                    "%s.%s[\n\t%s in %s\n] { }",
                    deviceIndicator, binding.getDevice(),
                    binding.getInterfaze(), binding.getNetwork()
            );
            bindingsString.append(bindingString).append("\n\n");
        });
        return bindingsString;
    }

    private Stage getStage() {
        return (Stage) inputTextField.getScene().getWindow();
    }

}