package gui;

public class NetworkInput {

    private final String name, ip, sub;

    public NetworkInput(String name, String ip, String sub) {
        this.name = name;
        this.ip = ip;
        this.sub = sub;
    }

    public String getName() {
        return name;
    }

    public String getIp() {
        return ip;
    }

    public String getSub() {
        return sub;
    }

}