package conf_generator;

import java.util.function.BiFunction;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public enum Variable {

    NTH("nth", true, (parameter, network) -> network.getNthAddress(parameter).toString()),
    FIRST("first", true, (parameter, network) -> network.getFirstHostAddress().toString()),
    LAST("last", true, (parameter, network) -> network.getLastHostAddress().toString()),
    NETADDR("netaddr", true, (parameter, network) -> network.getNetworkAddress().toString()),
    BROADADDR("broadaddr", true, (parameter, network) -> network.getBroadcastAddress().toString()),
    SUBADDR("subaddr", true, (parameter, network) -> network.getSubnetmaskAddress().toString()),
    SUBCIDR("subcidr", true, (parameter, network) -> network.getSubnetmaskCIDR() + ""),
    WILD("wild", true, (parameter, network) -> network.getWildcardAddress().toString()),
    NAME("name", false, (parameter, network) -> getDevice().getName());

    private final static String PREFIX = "\\$";
    private final static String PARAMS = "(?:\\((.*?)\\))?";
    private final static Pattern INTERFACE_PATTERN = Pattern.compile("int.*?\\s+(.*?)$", Pattern.MULTILINE + Pattern.CASE_INSENSITIVE);
    private final String name;
    private final Pattern variablePattern;
    private final boolean networkDependent;
    private final BiFunction<String, Network, String> replaceFunction;

    private static Device device;

    Variable(String name, boolean networkDependent, BiFunction<String, Network, String> replaceFunction) {
        this.name = name;
        this.networkDependent = networkDependent;
        this.replaceFunction = replaceFunction;

        this.variablePattern = Pattern.compile(PREFIX + this.name + PARAMS, Pattern.CASE_INSENSITIVE);

    }

    public Matcher getVariableMatcher(String input) {
        return this.variablePattern.matcher(input);
    }

    public static Matcher getInterfaceMatcher(String input) {
        return INTERFACE_PATTERN.matcher(input);
    }

    public boolean isNetworkDependent() {
        return this.networkDependent;
    }

    public String getReplacement(String params, Network network) {

        return this.replaceFunction.apply(params, network);

    }

    public static void setDevice(Device device_) {
        Variable.device = device_;
    }

    private static Device getDevice() {
        return device;
    }

}
