package conf_generator;

import java.util.regex.Matcher;

public class VariableConverter {

    private final static Variable[] VARIABLES = Variable.values();
    private final TopologyReader topologyReader;

    public VariableConverter(TopologyReader topologyReader) {
        this.topologyReader = topologyReader;
    }

    public String getConvertedConfig(Device device) {

        Variable.setDevice(device);

        String convertedConfig = device.getConfig();

        for(Variable variable : VARIABLES) {

            Matcher varMatcher = variable.getVariableMatcher(convertedConfig);

            while(varMatcher.find()) {

                final String parameters = varMatcher.group(1);

                if(variable.isNetworkDependent()) {
                    final int varPos = varMatcher.start();
                    final String allInterfaces = convertedConfig.substring(0, varPos);

                    Matcher interfaceMatcher = Variable.getInterfaceMatcher(allInterfaces);
                    String interfaze = "";
                    while(interfaceMatcher.find()) {
                        interfaze = interfaceMatcher.group(1);
                    }

                    interfaze = NetworkBinding.getShortVersion(interfaze);
                    final Network network = topologyReader.getNetworkByBinding(device, interfaze);

                    String replaceValue = variable.getReplacement(parameters, network);
                    convertedConfig = varMatcher.replaceFirst(replaceValue);

                } else {

                    convertedConfig = varMatcher.replaceFirst(variable.getReplacement(parameters, null));

                }

                varMatcher = varMatcher.reset(convertedConfig);

            }

        }

        return convertedConfig;

    }

}