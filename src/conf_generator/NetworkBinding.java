package conf_generator;

public class NetworkBinding {

    private final String deviceName;
    private final String interfaze;
    private final String networkName;
    private final static String INTERFACE_NETWORK_DELIMITER = "\\s+in\\s+";

    public NetworkBinding(String deviceName, String binding) {
        this.deviceName = deviceName;
        String[] splittedBinding = binding.split(INTERFACE_NETWORK_DELIMITER);
        this.interfaze = getShortVersion(splittedBinding[0]);
        this.networkName = splittedBinding[1];
    }

    public static String getShortVersion(String interfaze) {

        final String[] shortVersions = {"f ","g ","s ", "v "};
        final String interfazeBeginning = (interfaze.charAt(0) + "").toLowerCase() + " ";
        final String interfazePort = interfaze.replaceFirst("^[^\\d]*", "");

        for(String shortVersion : shortVersions) {
            if(interfazeBeginning.equals(shortVersion)) {
                return shortVersion + interfazePort;
            }
        }

        throw new IllegalArgumentException("Interface-Type is unknown: " + interfaze);

    }

    public String getDeviceName() {
        return deviceName;
    }

    public String getInterfaze() {
        return interfaze;
    }

    public String getNetworkName() {
        return networkName;
    }

    @Override
    public String toString() {
        return String.format(
                "Binding: {device-name: %s, interface: %s, network-name: %s}",
                this.getDeviceName(), this.getInterfaze(), this.getNetworkName()
        );
    }
}
