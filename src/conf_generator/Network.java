package conf_generator;

import com.github.maltalex.ineter.base.IPAddress;
import com.github.maltalex.ineter.base.IPv4Address;
import com.github.maltalex.ineter.base.IPv6Address;
import com.github.maltalex.ineter.range.IPSubnet;
import com.github.maltalex.ineter.range.IPv4Subnet;
import com.github.maltalex.ineter.range.IPv6Subnet;

import java.math.BigInteger;

public class Network {
    private final String name;
    private final IPSubnet ip;
    private final IPAddress broadcast;
    private final IPAddress wildcard;
    private final static String ADDRESS_DELIMITER = "/";

    public Network(String name, String fullAddress) {

        this.name = name;

        final String[] splittedAddress = fullAddress.trim().split(ADDRESS_DELIMITER);

        int maskLen = Integer.parseInt(splittedAddress[1]);

        if(IPAddress.of(splittedAddress[0]).version() == 4) {
            this.ip = IPv4Subnet.of(splittedAddress[0], maskLen);
            this.wildcard = ((IPv4Subnet) ip).getNetworkMask().not();
            this.broadcast = ((IPv4Subnet) ip).getNetworkAddress().or((IPv4Address) wildcard);
        } else {
            this.ip = IPv6Subnet.of(splittedAddress[0], maskLen);
            this.wildcard = ((IPv6Subnet) ip).getNetworkMask().not();
            this.broadcast = ((IPv6Subnet) ip).getNetworkAddress().or((IPv6Address) wildcard);
        }

    }


    private IPAddress plus(String n) {

        IPAddress addedIP;

        try {

            BigInteger toAdd = new BigInteger(n);
            BigInteger netIP = new BigInteger(
                    this.ip
                            .getNetworkAddress()
                            .toInetAddress()
                            .getAddress()
            );

            addedIP = IPAddress.of(
                    netIP
                        .add(toAdd)
                        .toByteArray()
            );

        } catch(NumberFormatException e) {
            throw new IllegalArgumentException("Input is not a number: " + n);
        }

        if(!isInRange(this.getNetworkAddress(), this.getBroadcastAddress(), addedIP)) {
            throw new IllegalArgumentException(String.format(
                    "%s is not in range of %s - %s", addedIP, this.getNetworkAddress(), this.getBroadcastAddress()
            ));
        }

        return addedIP;

    }

    private static boolean isInRange(IPAddress low, IPAddress high, IPAddress ip) {
        return isLargerOrEqualThan(ip, low) && isSmallerOrEqualThan(ip, high);
    }

    private static boolean isLargerOrEqualThan(IPAddress ip1, IPAddress ip2) {
        if(ip1.version() == 4 && ip2.version() == 4) {
            return ((IPv4Address) ip1).compareTo((IPv4Address) ip2) >= 0;
        }
        return ((IPv6Address) ip1).compareTo((IPv6Address) ip2) >= 0;
    }

    private static boolean isSmallerOrEqualThan(IPAddress ip1, IPAddress ip2) {
        if(ip1.version() == 4 && ip2.version() == 4) {
            return ((IPv4Address) ip1).compareTo((IPv4Address) ip2) <= 0;
        }
        return ((IPv6Address) ip1).compareTo((IPv6Address) ip2) <= 0;
    }

    public IPAddress getNetworkAddress() {
        return this.ip.getNetworkAddress();
    }

    public IPAddress getBroadcastAddress() {
        return this.broadcast;
    }

    public IPAddress getNthAddress(String n) {
        return this.plus(n);
    }

    public IPAddress getFirstHostAddress() {
        return this.getNetworkAddress().plus(1);
    }

    public IPAddress getLastHostAddress() {
        return this.getBroadcastAddress().minus(1);
    }

    public String getName() {
        return this.name;
    }

    public IPAddress getSubnetmaskAddress() {
        return this.ip.getNetworkMask();
    }

    public int getSubnetmaskCIDR() {
        return this.ip.getNetworkBitCount();
    }

    public IPAddress getWildcardAddress() {
        return this.wildcard;
    }


    @Override
    public String toString() {
        return String.format(
                "Network: {name: %s, range: %s - %s / %s}",
                this.getName(), this.getNetworkAddress(), this.getBroadcastAddress(), this.ip.getNetworkBitCount()
        );
    }

}