package conf_generator;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.regex.Pattern;

public class IOManager {

    private final static Pattern EMPTY_LINES_AND_TABS = Pattern.compile("^\\s+", Pattern.MULTILINE);

    public static void main(String[] args) {

        String inputPath, outputDir;

        if(args.length == 0) {
            Scanner in = new Scanner(System.in);
            System.out.println("Please enter the absolute Path to your input-file!");
            inputPath = in.nextLine();

            System.out.println("Please enter the absolute Path to your output-directory!");
            outputDir = in.nextLine();
        } else {
            inputPath = args[0];
            outputDir = args[1];
        }

        String input;
        try {
            input = new String(Files.readAllBytes(Paths.get(inputPath)), StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new IllegalArgumentException("Couldn't read the file with the path: " + inputPath);
        }


        TopologyReader tr = new TopologyReader(input);
        VariableConverter vc = new VariableConverter(tr);

        String finalOutputDir = outputDir;
        tr.getDevices().forEach(device -> {

            File deviceFile = new File(finalOutputDir + "\\" + device.getName() + ".txt");
            String deviceConf = vc.getConvertedConfig(device);
            deviceConf = EMPTY_LINES_AND_TABS.matcher(deviceConf).replaceAll("");
            try {
                Files.createFile(deviceFile.toPath());
            } catch (IOException e) {
                throw new IllegalArgumentException("Could not create file with the path: " + deviceFile.getAbsolutePath());
            }

            try {
                Files.write(deviceFile.toPath(), deviceConf.getBytes());
            } catch (IOException e) {
                throw new IllegalArgumentException("Could not write to file with the path: " + deviceFile.getAbsolutePath());
            }

        });


    }

}
