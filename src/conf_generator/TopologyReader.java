package conf_generator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TopologyReader {

    private final List<Network> networks = new ArrayList<>();
    private final List<Device> devices = new ArrayList<>();
    private final List<NetworkBinding> networkBindings = new ArrayList<>();

    private final static String BLOCK_DELIMITER = "}\\s*";
    private final static String HEADCONF_DELIMITER = "\\{";
    private final static String DEVICES_DELIMITER = ",\\s*";
    private final static String TYPE_DELIMITER = "\\.";
    private final static String NETWORK_STRING = "Net";
    private final static String NAME_DELIMITER = "\\s*\\[";
    private final static String BINDINGS_DELIMITER = "[\\n\\r]+";

    public TopologyReader(String input) {

        String[] blocks = input.split(BLOCK_DELIMITER);

        for (String block : blocks) {
            String[] headconf = block.split(HEADCONF_DELIMITER);
            String header = headconf[0];
            String config = headconf[1];

            String[] devices = header.split(DEVICES_DELIMITER);

            for (String device : devices) {
                String[] splittedType = device.split(TYPE_DELIMITER);

                String type = splittedType[0];
                String afterType = splittedType[1];

                String[] splittedName = afterType.split(NAME_DELIMITER);

                String name = splittedName[0].trim();
                String bindings = "";
                if (splittedName.length > 1) {
                    bindings = splittedName[1];
                }

                List<String> seperatedBindings = new ArrayList<>();
                if (!bindings.trim().isEmpty()) {
                    String cleanBindings = bindings.trim().substring(0, bindings.trim().length() - 1);
                    String[] splittedBindings = cleanBindings.split(BINDINGS_DELIMITER);
                    seperatedBindings = Arrays.asList(splittedBindings);
                }

                if (isNetwork(type)) {

                    this.networks.add(
                            new Network(name, config)
                    );

                } else {

                    Device originalDevice = this.getDeviceByName(name);
                    if(originalDevice != null) {
                        originalDevice.setConfig(originalDevice.getConfig() + "\n" + config.replaceAll("^[\\r\\n]+", ""));
                    } else {
                        this.devices.add(
                                new Device(name, config)
                        );
                    }

                    seperatedBindings.forEach(networkBinding -> {
                        if(networkBinding.trim().isEmpty()) {
                            return;
                        }
                        networkBindings.add(
                                new NetworkBinding(name, networkBinding)
                        );

                        System.out.println(networkBindings);

                    });

                }

            }

        }

    }

    private boolean isNetwork(String device) {
        return device.equals(NETWORK_STRING);
    }

    private Device getDeviceByName(String name) {
        for(Device device : this.devices) {
            if(device.getName().equals(name)) {
                return device;
            }
        }
        return null;
    }

    public Network getNetworkByBinding(Device device, String interfaze) {

        System.out.println(device);
        System.out.println(interfaze);
        System.out.println(this.networkBindings);

        for(NetworkBinding networkBinding : this.networkBindings) {

            System.out.println(networkBinding);

            if(
                    networkBinding.getDeviceName().equals(device.getName()) &&
                            networkBinding.getInterfaze().equals(interfaze)
            ) {
                return this.getNetworkByName(networkBinding.getNetworkName());
            }

        }

        throw new IllegalArgumentException(
                String.format(
                        "There is no Network bound to the interface %s of the device %s",
                        interfaze, device.getName()
                )
        );

    }

    private Network getNetworkByName(String name) {
        for(Network network : this.networks) {
            if(network.getName().equals(name)) {
                return network;
            }
        }

        throw new IllegalArgumentException(
                String.format(
                        "There is no conf_generator.Network by the name of \"%s\"",
                        name
                )
        );

    }

    public List<Network> getNetworks() {
        return networks;
    }

    public List<Device> getDevices() {
        return devices;
    }

    public List<NetworkBinding> getNetworkBindings() {
        return networkBindings;
    }

    @Override
    public String toString() {
        return String.format(
                    "=====devices=====\n" +
                    "%s\n" +
                    "=================\n\n" +
                    "=====networks=====\n" +
                    "%s\n" +
                    "==================\n\n" +
                    "=====BINDINGS=====\n" +
                    "%s\n" +
                    "==================",
                this.devices, this.networks, this.networkBindings
        );
    }
}
