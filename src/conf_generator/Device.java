package conf_generator;

public class Device {

    private final String name;
    private String config;

    public Device(String name, String config) {
        this.name = name;
        this.config = config;
    }

    public String getName() {
        return this.name;
    }

    public String getConfig() {
        return this.config;
    }

    public void setConfig(String config) {
        this.config = config;
    }

    @Override
    public String toString() {
        return String.format(
                "Device: {name: %s, config: %s}",
                this.getName(), this.getConfig()
        );
    }
}
